let student = [];

function addStudent(name) {
	student.push(name)
	console.log(name + " was added to the student's list.")
}

function countStudents() {
	let numberOfStudents = student.length;
	console.log ("There are a total of " + numberOfStudents + " students enrolled.");
}

function printStudents(name) {
	student.sort();
	student.forEach(name => console.log(name));
}


// Stretch Goals:


function findStudent(name) {

	let firstChar = name.slice(0,1);

	let upperCaseFirstChar = firstChar.toUpperCase();

	let restOfName = name.slice(1,name.length);

	let capitalizedName = upperCaseFirstChar + restOfName;
	
	let filteredStudents = student.filter(
			function(name) {
				return name.toLowerCase().includes('j');
			}
	)
	let join = filteredStudents.join()

	if (name === "j") {
		console.log(join + " are enrollees")
	}

	else if (name.toLowerCase() === "jane" || name.toLowerCase() === "john" || name.toLowerCase() === "joe") {
		console.log(capitalizedName + " is an Enrollee");

	} else {
		console.log("No student found with the name " + capitalizedName);
	}

}


