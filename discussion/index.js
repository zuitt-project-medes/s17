// alert("hi")

// common examples of Arrays

let grades = [98.5, 94.3, 89.2, 90.1];

let marvelHeroes = ["Iron Man", "Captain America", "Thor", "Hulk", "Black Widow", "Hawkeye", "Shang Chi", "Spider Man"];

// possible use of an array but not recommended

let mixedArray = [12, "Asus", null, undefined, {}];

// alternative way to write an array

let myTasks = [
	"drink HTML",
	"eat Javascript",
	"inhale CSS",
	"bake React"
];

// arrays and indexes

// [] - array literals
// index starts with 0

// reassign values in an array

console.log(myTasks);
myTasks[0] = "sleep for 8 hours";
console.log(myTasks);

// accessing from an array
console.log(grades[2]);
// result: 89.2
console.log(marvelHeroes[6]);
// result: Shang Chi
console.log(myTasks[20]);
// result: undefined

// getting the length of an array
// arrays have access to ".length" property to get the number of elements present in an array

console.log(marvelHeroes.length);
// result: 8


// this is useful for executing code hat depends on the content of our array
if(marvelHeroes.length > 5){
	console.log("We have too many heroes, please contact Thanos");
}

// accessing the last element of an array
// since the first element of an array starts with 0, subtracting 1 to the length of an array will offset the value by one allowing us to get the last element

let lastElement = marvelHeroes.length-1;
console.log(marvelHeroes[lastElement]);
// result: Spider Man

// array methods

	// mutator method
		// these are functions that mutate or change an array after they are created

let fruits = ["Apple", "Blueberry", "Orange", "Grapes"];

// push
	// adds an element in the end of an array and returns the array's length

console.log(fruits);

let fruitsLength = fruits.push("Mango");
console.log(fruits);

// adding multiple elements
fruits.push("Guava", "Kiwi");
console.log(fruits);

// pop()

// removes the last element in an array and returns the removed element

let removeFruit = fruits.pop();
console.log(removeFruit);
console.log(fruits);

fruits.pop();
console.log(fruits);

// unshift()

// adds an element/s at the beginning of an array

fruits.unshift("Guava");
console.log(fruits);

fruits.unshift("Kiwi", "Lime");
console.log(fruits);

// shift()

// removing an element/s at the beginning of an array

fruits.shift();
console.log(fruits);

/*
note:

push and unshift- adding elements

pop and shift- removing elements

*/

// splice()
// simultaneously removes elementsnfrom a specified index number and adds elements
/*
Syntax:

arayName.splice(startingIndex, deleteCount, elementsToBeAdded);

*/

fruits.splice(1, 2, "Cherry", "Watermelon");
console.log(fruits);

// removes from index 3 to the last element
fruits.splice(3)
console.log(fruits);

// removing elements in the middle
fruits.splice(1, 1);
console.log(fruits);

// adding in the middle of an array
fruits.splice(2, 0, "Cherry", "Buko");
console.log(fruits);

// sort()

// rearranges the array elements in alphanumeric order

fruits.sort();
console.log(fruits);

// reverse()

// reverses the order of the array elements

fruits.reverse();
console.log(fruits);

// non-mutator methods

// these functions do not modify or change an array

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"]

// indexOf()
// returns the index number of the first matching element found in an array
console.log(countries.indexOf("PH"));
// result: 1

let indexOfSG = countries.indexOf("SG");
console.log(indexOfSG);
// result: 3

let invalidCountry = countries.indexOf("SK");
console.log(invalidCountry);
// result: -1

let invalidCountry2 = countries.indexOf("JP");
console.log(invalidCountry2);
// result: -1

// console.log(countries);

// slice()

// slices elements from an array and returns a new array

/*
Syntax:

arrayName.slice(startingIndex);
arrayName.slice(startingIndex, endingIndex);

*/

// slice off elements from a specified index to the last element

let slicedArrayA = countries.slice(2);
console.log(slicedArrayA);
// result: ['CAN', 'SG', 'TH', 'PH', 'FR', 'DE']


let slicedArrayB = countries.slice(2, 4);
console.log(slicedArrayB);
// result: ['CAN', 'SG']

// slice off elements starting from the last element of an array

let slicedArrayC = countries.slice(-3);
console.log(slicedArrayC);
// result: ['PH', 'FR', 'DE']

// toString()
// returns an array as string separated by commas
// Syntax: arrayName.toString();

let stringArray = countries.toString();
console.log(stringArray);
// result: US,PH,CAN,SG,TH,PH,FR,DE

let sentence = ["I", "like", "javascript", ".", "It's", "fun", "!"]

let sentenceString = sentence.toString();
console.log(sentenceString);
// result: I,like,javascript,.,It's,fun,!

// concat()

// combines two arrays and returns a combined result
// Syntax: arrayA.concat(concatB);
// arrayA.concat(elementA);


let taskArrayA = ["drink HTML", "eat javascript"];
let taskArrayB = ["inhale CSS", "breathe SASS"];
let taskArrayC = ["get GIT", "be NODE"];

let tasks = taskArrayA.concat(taskArrayB);
console.log(tasks);

// combining multiple arrays

let allTasks = taskArrayA.concat(taskArrayB, taskArrayC);
console.log(allTasks);

// combining array with elements

let combinedTasks = taskArrayA.concat("smell Express", "throw React");
console.log(combinedTasks);

// console.log(taskArrayA);

// join()
// returns an array as a string separated by a specified separator string

let members = ["Rose", "Lisa", "Jisoo", "Jeanie"];

let joinedMembers1 = members.join();
console.log(members);
// result: comma as separator

let joinedMembers2 = members.join('');
console.log(joinedMembers2);
// result: RoseLisaJisooJeanie no space

let joinedMembers3 = members.join(" ");
	console.log(joinedMembers3);
// result: Rose Lisa Jisoo Jeanie - with space separator

let joinedMembers4 = members.join("/");
	console.log(joinedMembers4);
// result: Rose/Lisa/Jisoo/Jeanie - with / separator

// iteration method
// they're designed to perform repetitive tasks on arrays
// useful formanipulating array data resulting in complex tasks

// forEach()

// an empty array will store the filtered elements in the iteration method. This is to avoid confusion by modifying the original array.

let filteredTasks = [];

allTasks.forEach(function(task){
	console.log(task);

	if(task.length > 10){
		filteredTasks.push(task);
	}
});

console.log(filteredTasks);


// map()

// iterates on each element and returns new array with different values

// unlike forEach method, map method requires the use of return keyword in order to create another array with the performed operation

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;

});

console.log(numberMap);

// every()
// will check if all elements meet a certain condition
// returns a true value if all elements meet the condition and false if otherwise


let allValid = numbers.every(function(number){
	return(number < 3 && number <= 5);
})
console.log(allValid);

// some()
// checks atleast one element in the array that meets the condition
// returns a true value if at least one element meets the condition, false if otherwise

let someValid = numbers.some(function(number){
	return(number < 2);
});

console.log(someValid);
// result: true

// filter()
// returns a new array that contains the elements that meets a certain condition
// if there is no elements found, it will return an empty array

let filterValid = numbers.filter(function(number){
	return(number < 3);
});
console.log(filterValid);
// result: [1, 2]

let colors = ["red", "green", "black", "orange", "yellow"];

let values = ["red", "black", "yellow"];

colors = colors.filter(item => values.indexOf(item) === -1)

console.log(colors);

/*
colors = colors.filter(function(item){
	return(values.indexOf(item)=== -1)
})
*/

// includes()
// 

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"]

let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
});

console.log(filteredProducts);

// multidimensional array
// useful for storing complex data structures

let chessboard = [
	["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
	["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
	["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
	["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
	["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
	["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
	["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
	["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"],
]

console.log(chessboard[1][4]);
console.log(`Queen moves to ${chessboard[5][7]}`)



















